# PongVr
Pong for virtual reality

![Screenshot 1](screenshots/PongVr.webp)

Play with arrows , WASD or Vr Joystick


## Story

"In the year 2178, humanity has colonized the farthest reaches of the galaxy, but with great expansion comes great conflict. Enter the neon-lit arena of Galactic Pong, where players must battle against their rival factions using advanced ping-pong technology, but beware - your opponent isn't just any ordinary foe; they are powered by an intelligence AI assistant named Nova, who will stop at nothing to ensure victory for her player.

## Contributing

I'm looking for help in the following areas:

- Development
  - Unresolved issues will be posted on issues, pull request to solve them are welcome, if an issue is not opened please open it first so that the provided solution can be explained and dicussed first.

- Modeling and texturing

- Testing
  - Betatesting, reporting bugs and providing feedback helps to improve this software.

- Feedback
  - Feedback regarding any part of the game is welcome and helpful, feel free to comment on any aspect you want to be improved.

# Credits

	[![Credits](https://codeberg.org/glitchapp/pong-vr/src/branch/master/credits.md)]

# License

 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

The libraries on the lib folder belong to their authors and have their own licenses which can be found on their folders.

# Donations

If you like this remake and you want to support the development - please consider to donate. Any donations are greatly appreciated.

![Zcash address:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/Zc.webp) t1h9UejxbLwJjCy1CnpbYpizScFBGts5J3N
![Zcash Qr Code:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/ZCashQr160.webp)
