function loadVoices()
  
    Voice1 = lovr.audio.newSource("voice/one.ogg")
    Voice2 = lovr.audio.newSource("voice/two.ogg")
    Voice3 = lovr.audio.newSource("voice/three.ogg")
    Voice4 = lovr.audio.newSource("voice/four.ogg")
    Voice5 = lovr.audio.newSource("voice/five.ogg")
    Voice6 = lovr.audio.newSource("voice/six.ogg")
    Voice7 = lovr.audio.newSource("voice/seven.ogg")
    Voice8 = lovr.audio.newSource("voice/eight.ogg")
    Voice9 = lovr.audio.newSource("voice/nine.ogg")
    
    HumanIsWinning = lovr.audio.newSource("voice/humaniswinning.ogg")
    CPUIsWinning = lovr.audio.newSource("voice/CPUiswinning.ogg")
    pointsVoice = lovr.audio.newSource("voice/points.ogg")
    
    matchStartedVoice = lovr.audio.newSource("voice/matchStarted.ogg")
    
    CrowdCheering = lovr.audio.newSource("sounds/CrowdCheering.ogg")
    CrowdCheering:setVolume(0.3)
    
    VoiceStep = 0
    sentence5PoinsSaid = false
    sentence10PoinsSaid = false
    sentence15PoinsSaid = false
    sentence20PoinsSaid = false
    
    ismusicplaying = false
    isplayerlostplaying = false

end

function updateVoices(dt)
	if timer<5 then
		if firstGameStarted==true then
				if timer>1 and VoiceStep==0	then Voice3:play()  VoiceStep=1
			elseif timer>2 and VoiceStep==1 then Voice2:play()	VoiceStep=2
			elseif timer>3  and VoiceStep==2 then Voice1:play()	VoiceStep=3
			elseif timer>4  and VoiceStep==3 then matchStartedVoice:play()	VoiceStep=4
			end
	
		end
	
	end
	if VoiceStep==4 then
		updateBall(dt)
	end
	
	if player1.score>4 and sentence5PoinsSaid==false then
		sayWhosWinning()
		sentence5PoinsSaid=true
			--increase speed
			initialTimeScale = initialTimeScale + timeScaleIncrement
			currentTimeScale = initialTimeScale
			ball.speedx = -250*currentTimeScale
			ball.speedy = -500*currentTimeScale
			
	elseif player1.score>9 and sentence10PoinsSaid==false then
		sayWhosWinning()
		sentence10PoinsSaid=true
  end
end
