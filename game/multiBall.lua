
-- Variables for multi-ball mode and custom ball shape
local multiBallEnabled = false  -- Toggle multi-ball mode


-- Function to create a new ball
function createBall()
    local newBall = {
        x = world.width / 2,
        y = world.height / 2,
        radius = 10,  -- Adjust the ball size
        speedx = initialBallSpeedX,
        speedy = initialBallSpeedY,
    }
    return newBall
end

-- Initialize a list of balls with a single default ball
local balls = {createBall()}


function updateMultiBall(dt)

   -- Multi-ball mode
    if multiBallEnabled then
        for _, ball in ipairs(balls) do
            ball.x = ball.x + ball.speedx * dt

            -- Ball behavior
            if ball.y < 30 then
                ball.y = ball.y + 20
                ball.speedy = initialBallSpeedY
            end
            if ball.y > world.height + 50 then
                ball.y = ball.y - 20
                ball.speedy = -initialBallSpeedY
            end
        end
    else
        -- Single ball behavior
        local ball = balls[1]
        ball.x = ball.x + ball.speedx * dt

        if ball.y < 30 then
            ball.y = ball.y + 20
            ball.speedy = initialBallSpeedY
        end
        if ball.y > world.height + 50 then
            ball.y = ball.y - 20
            ball.speedy = -initialBallSpeedY
        end
    end

end
