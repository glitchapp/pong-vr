function loadDayNightCycle()


dayDuration = 7200  -- Adjust to match the planet's day length in seconds
dayDurationSeconds = 7200  -- Adjust to match the planet's day length in seconds

sunriseDuration = 300 -- -- Define sunriseDuration (in seconds) and Adjust it


-- Define variables for temperature and temperature oscillation range
baseTemperature = 20 -- Base temperature
temperatureRange = 10 -- Oscillation range

end

function updateDayNightCycle(dt)

-- Calculate the time of day (0 to 1)
timeOfDay = timer / dayDuration
timeOfDay = math.fmod(timeOfDay, 1)  -- Wrap around for a continuous day/night cycle

-- Calculate the sun's position based on time of day
local sunAngle = math.pi * 2 * timeOfDay

-- Calculate the intensity of the sun's light
local sunIntensity = 1

 -- Calculate the temperature based on daylight
  local temperature = baseTemperature + math.sin(sunAngle) * temperatureRange

  -- Update the temperature display
  temperatureText = string.format("Temp: %.2f°C", temperature)
  

local transitionFactor = 0.1  -- Adjust this value for the speed of the transition
local red, green, blue

--print(timeOfDay)
if timeOfDay < sunriseDuration / dayDuration then
    -- Simulate a unique daytime to dusk transition
    local t = timeOfDay / (sunriseDuration / dayDuration)
    red = math.max(0, 1 - t)  -- Linear decrease in red, clamped at 0
    green = math.min(1, math.max(0, 0.4 + t * 1.5)) -- Linear increase in green, clamped within [0, 1]
    blue = math.min(1, math.max(0, 0.4 + t * 1.5)) -- Linear increase in blue, clamped within [0, 1]
elseif timeOfDay >= (1 - sunriseDuration / dayDuration) then
    -- Simulate a unique night to dawn transition
    local t = (timeOfDay - (1 - sunriseDuration / dayDuration)) / (sunriseDuration / dayDuration)
    red = math.min(1, math.max(0, 0.4 + t * 1.5)) -- Linear increase in red, clamped within [0, 1]
    green = math.min(1, math.max(0, 0.4 + t * 1.5)) -- Linear increase in green, clamped within [0, 1]
    blue = math.min(1, math.max(0, 0.4 + t * 1.5)) -- Linear increase in blue, clamped within [0, 1]
else
    -- Simulate nighttime
    red = 0.1  -- Very dark red
    green = 0.1  -- Very dark green
    blue = 0.1  -- Very dark blue
end


--print("red: " .. red .. " green: " .. green .. " blue: " .. blue)


skymapColor = {red, green, blue, 1}  -- Store the sky color in the skymapColor table
--[[
if not (slowMotionActive==true) then
		skymapColor = {red, green, blue, 1}  -- Store the sky color in the skymapColor table
elseif slowMotionActive==true then
		 skymapColor = {red-DesaturateColor.r/6, green-DesaturateColor.g/6, blue-DesaturateColor.b/6, 1}
end
--]]
		
    -- Update the skybox texture based on time of day
    --local skyboxTexture = background1tex
    --[[if timeOfDay > 0.5 or timeOfDay < 0.01 then
        skyboxTexture = background1Nighttex  -- Daytime skybox
    else
        skyboxTexture = background1tex  -- Nighttime skybox
    end--]]
	
    --lovr.graphics.setColor(skymapColor)  -- Set the color using the skymapColor table
    --lovr.graphics.skybox(skyboxTexture)

end
