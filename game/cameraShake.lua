function updateCameraShake(dt)
 -- Update camera shake effect
    if cameraShakeTimer > 0 and cameraShakeTimer < 1 then
        CameraOffset = {
            x = math.random() * cameraShakeIntensity,
            y = math.random() * cameraShakeIntensity,
            z = math.random() * cameraShakeIntensity
        }
        
        cameraShakeTimer = cameraShakeTimer - dt
    elseif cameraShakeTimer> 1 then
        cameraShakeTimer = 0
    end
end
