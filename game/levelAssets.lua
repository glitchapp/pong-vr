function loadLevelAssets()
  -- Load textures and create materials
    blockpng = lovr.graphics.newTexture("/assets/block.png")
    blockmat = lovr.graphics.newMaterial(blockpng, 1, 0, 1, 0.3)
    
    boxpng = lovr.graphics.newTexture("/assets/box.png")
    boxmat = lovr.graphics.newMaterial(boxpng, 1, 0, 1, 0.3)
    boxmatcei = lovr.graphics.newMaterial(boxpng, 0, 0, 1, 1)
    boxmatflo = lovr.graphics.newMaterial(boxpng, 0, 1, 1, 0.3)
    boxmatshadow = lovr.graphics.newMaterial(boxpng, 1, 1, 1, 0.3)
    
    background1tex = lovr.graphics.newTexture("/assets/1.jpg")
    --background1Nighttex = lovr.graphics.newTexture("/assets/1night.jpg")
    --skyboxTexture = background1Nighttex  -- Daytime skybox

   
  
end
