function loadWorldSettings()
 -- Initialize world settings
    world = {}
    world.height = 800
    world.width = 800
    
    player1 = {}
    player1.x = 10
    player1.y = 250
    player1.width = 25
    player1.height = 100
    player1.depth = 100
    player1.colorr = 1
    player1.colorg = 0.1
    player1.colorb = 0.1
    player1.score = 0
    player1.baseSpeed = 800 -- Initial speed
    player1.speed = player1.baseSpeed -- Current speed
    player1.inertia = 0.95 -- Inertia factor
    player1.y = world.height / 2
    player1.height = 100 -- Adjust this value as needed
    maxPlayerSpeed = 1000
    
    player2 = {}
    player2.x = 765
    player2.y = 250
    player2.width = 25
    player2.height = 100
    player2.depth = 100
    player2.colorr = 0.1
    player2.colorg = 0.1
    player2.colorb = 1
    player2.speed = 50
    player2.score = 0
    player2.baseSpeed = 800 -- Initial speed
    player2.speed = player1.baseSpeed -- Current speed
    player2.inertia = 0.95 -- Inertia factor
    player2.y = world.height / 2
    player2.height = 100 -- Adjust this value as needed
    maxPlayerSpeed = 1000
    player2.isPlaying = "f"
    
    ball = {}
    ball.x = 375
    ball.y = 275
    ball.width = 25
    ball.height = 25
    
    initialTimeScale = 1  -- The initial time scale (normal speed)
    currentTimeScale = initialTimeScale
    timeScaleIncrement = 0.1  -- Adjust this value to control the speed increase rate
    
    ball.speedx = -250 * currentTimeScale
    ball.speedy = -500 * currentTimeScale
    ball.prex = 375
    ball.prey = 275
end
