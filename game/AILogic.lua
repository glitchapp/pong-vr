function loadAILogic()

-- Constants
cpuInertia = 0.5 -- Adjust the value to control the amount of inertia
randomnessFactor = 30 -- Adjust the value to control the randomness

-- Define AI difficulty levels
AI_DIFFICULTY = {
    EASY = 1,
    MEDIUM = 2,
    HARD = 3,
}

-- Initialize AI difficulty
currentAIDifficulty = AI_DIFFICULTY.EASY

gameProgress=1

end



function updateAILogic(dt)

  if loseV == "f" and player2.isPlaying == "f" then
        -- Calculate AI's ideal position to hit the ball
        local targetY = ball.y

        -- Adjust the targetY based on ball speed and direction
        if ball.speedx > 0 then
            targetY = targetY + ball.speedy * (world.width - 100 - ball.x) / ball.speedx
        end

        -- Add some randomness to the AI's movement
        local randomness = math.random(-randomnessFactor, randomnessFactor)
        targetY = targetY + randomness

        -- Limit the targetY within the game world boundaries
        targetY = math.max(30, math.min(targetY, world.height - 30))

        -- Apply inertia to the AI's movement
        player2.y = player2.y + (targetY - player2.y) * cpuInertia

        -- Adjust AI difficulty based on the game's progress
        if gameProgress >= 10 then
            --currentAIDifficulty = AI_DIFFICULTY.MEDIUM
        end
        if gameProgress >= 20 then
            --currentAIDifficulty = AI_DIFFICULTY.HARD
        end

        -- Modify AI's behavior based on difficulty
        if currentAIDifficulty == AI_DIFFICULTY.MEDIUM then
            -- More precise movement
            player2.y = player2.y + (targetY - player2.y) * cpuInertia * 2
        elseif currentAIDifficulty == AI_DIFFICULTY.HARD then
            -- Even more precise movement and faster reaction
            player2.y = player2.y + (targetY - player2.y) * cpuInertia * 3
        end
    end

end
