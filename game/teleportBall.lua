function loadTeleportBall()
	-- Variables for custom ball trajectories and teleportation
	teleportCounter = 0
	maxTeleportCount = 2  -- Adjust as needed
	customTrajectoryEnabled = false  -- Deactivate custom trajectories
	customTrajectoryTimer = 0
	customTrajectoryDuration = 5  -- Adjust the duration of the custom trajectory
	
	teleportCooldown = 1  -- Adjust the cooldown duration (in seconds)
	teleportTimer = teleportCooldown  -- Initialize the timer
end

function updateTeleportBall(dt)

 customTrajectoryTimer = customTrajectoryTimer + dt

        -- Implement custom behavior here
        if customTrajectoryTimer < customTrajectoryDuration then
            -- Example: Custom trajectory behavior
            ball.speedx = 200  -- Adjust the custom x-speed
            ball.speedy = 100  -- Adjust the custom y-speed
        else
            -- Return to normal behavior
            ball.speedx = initialBallSpeedX
            ball.speedy = initialBallSpeedY
        end
        
         -- Custom teleportation behavior
    teleportTimer = teleportTimer + dt
    if customTrajectoryEnabled == true and teleportCounter >= maxTeleportCount and teleportTimer >= teleportCooldown then
        teleportCounter = 0

        -- Implement teleportation logic here
        ball.x = math.random(50, world.width - 50)
        ball.y = math.random(50, world.height - 50)

        -- Reset the teleport timer
        teleportTimer = 0
    else
        teleportCounter = teleportCounter + 1
    end

end
