function loadSlowMotion()
  slowMotionDuration = 0.5
    slowMotionTimer = 0
      
  -- Define a flag to track if a slow-motion effect is active
	slowMotionActive = false
	
	
	--Desaturate the scene
	DesaturateColor = {}
	DesaturateColor.r = 0.2126
	DesaturateColor.g = 0.7152
	DesaturateColor.b = 0.0722
end


function updateSlowMotion(dt)

 -- Reset the slow-motion effect flag
    if slowMotionActive==true then
    
		-- Update the slow-motion timer
		slowMotionTimer = slowMotionTimer + dt

		-- Check if the slow-motion duration has passed
		if slowMotionTimer >= slowMotionDuration then
			slowMotionActive = false  -- Disable slow motion
			slowMotionTimer = 0  -- Reset the timer
			currentTimeScale = initialTimeScale  -- Reset time scale to normal speed
		end
		
    end
 end


