-- Constants
local initialBallSpeedX = 300
local initialBallSpeedY = 500
local maxBallSpeedX = 1000
local maxBallSpeedY = 800
local accelerationFactor = 1.1 -- Adjust this value to control the rate of acceleration



-- Variables for custom ball shape
local ballShape = "circle"  -- Set default ball shape (can be "circle" or "square")

require "game/AILogic"
loadAILogic()
require "game/teleportBall"
loadTeleportBall()


local ballShape = "circle"  -- Set default ball shape (can be "circle" or "square")
-- ...

function updateBall(dt)
    checkCollP1()
    checkCollP2()

    -- Custom ball trajectories and teleportation
    if customTrajectoryEnabled then
       
       updateTeleportBall(dt)	-- code in game/teleportBall.lua
       
    else
        ball.x = ball.x + ball.speedx * dt

        -- Ball behavior
        if ball.y < 30 then
            ball.y = ball.y + 20
            ball.speedy = initialBallSpeedY
        end
        if ball.y > world.height + 50 then
            ball.y = ball.y - 20
            ball.speedy = -initialBallSpeedY
        end
    end

    -- Apply acceleration when hit
    if checkCollP2() then
        ball.speedx = math.min(maxBallSpeedX, ball.speedx * accelerationFactor)
        ball.speedy = math.min(maxBallSpeedY, ball.speedy * accelerationFactor)
    end
	if ball.speedx==nil then ball.speedx=300 end
    ball.x = ball.x + ball.speedx * dt
    ball.y = ball.y + ball.speedy * dt

    if loseV == "f" then
        if player2.isPlaying == "f" then
            -- Calculate AI's ideal position to hit the ball
            local targetY = ball.y

            -- Adjust the targetY based on ball speed and direction
            if ball.speedx > 0 then
                targetY = targetY + ball.speedy * (world.width - 100 - ball.x) / ball.speedx
            end

            -- Add some randomness to the AI's movement
            local randomness = math.random(-randomnessFactor, randomnessFactor)
            targetY = targetY + randomness

            -- Limit the targetY within the game world boundaries
            targetY = math.max(30, math.min(targetY, world.height - 30))

            -- Apply inertia to the AI's movement
            player2.y = player2.y + (targetY - player2.y) * cpuInertia
        end
    end

    ball.prex = x
    ball.prey = y

   
end



ReversesEnergy= lovr.audio.newSource( "sounds/ReversesEnergy01.ogg")
ReversesEnergy2= lovr.audio.newSource( "sounds/ReversesEnergy02.ogg")

function checkCollP1()
  flag = 0
  
  if ball.speedy < 1 then
    flag = 25
  end 
  
  if ball.x < 0 then
    if loseV == "f" then
      player2.score = player2.score + 1
    end
    loseV = "1"
    lose()
  end
  if ball.x < 35 and ball.x > 5 then
    if ball.y > player1.y - flag and ball.y < player1.y + player1.height then
      ball.x = ball.x + 10
      
      ball.speedx = 250
	
	slowMotionActive=true
	cameraShakeTimer=0.1
	ReversesEnergy:play()
	
       return true -- Collision detected, return true
    end
  end
  return false -- No collision detected, return false
end

function checkCollP2()
  flag = 0
  if ball.speedy==nil then ball.speedy=300 end
  if ball.speedy < 1 then
    flag = 25
  end 
  
  if ball.x > world.width then
    if loseV == "f" then
      player1.score = player1.score + 1
    end
    loseV = "2"
    lose()
  end
  if ball.x > world.width - 55 and ball.x < 755 then
    if ball.y > player2.y - flag and ball.y < player2.y + player2.height then
      ball.x = ball.x
      ball.speedx = -250
      slowMotionActive=true
      cameraShakeTimer=0.1
      ReversesEnergy2:play()
      --timer=0
      return true -- Collision detected, return true
    end
  end
  
  return false -- No collision detected, return false
end



