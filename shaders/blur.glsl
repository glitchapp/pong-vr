// Vertex shader
out vec4 position;
out vec2 lovrTexCoord;

void lovrVert() {
    lovrTexCoord = lovrTexCoord0;
    gl_Position = lovrModel * vec4(lovrPosition, 1.0);
}

// Fragment shader
out vec4 fragColor;

uniform sampler2D lovrDiffuse;
uniform vec2 resolution;
uniform float blurAmount = 0.005;

void main() {
    vec4 color = vec4(0.0);
    vec2 offset = vec2(0.0, blurAmount);

    for (float x = -2.0; x <= 2.0; x += 1.0) {
        for (float y = -2.0; y <= 2.0; y += 1.0) {
            vec2 uv = lovrTexCoord + vec2(x, y) * offset;
            color += texture(lovrDiffuse, uv / resolution);
        }
    }

    color /= 25.0; // Adjust for the kernel size
    fragColor = color;
}
