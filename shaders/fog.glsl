out vec4 fragColor;

uniform sampler2D lovrDiffuse; // The original scene texture
uniform float fogDensity; // Adjust this to control the fog density

void main() {
    vec4 color = texture(lovrDiffuse, lovrVertexTexCoord);

    // Calculate fog based on the depth value (lovrDepth)
    float depth = lovrDepth;
    vec4 fogColor = vec4(0.7, 0.7, 0.7, 1.0); // Adjust the color
    color = mix(fogColor, color, exp(-depth * fogDensity));

    fragColor = color;
}

