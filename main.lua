-- Import required modules
require "checkball"
require "shadows"
require "keys"
require "VrJoystick"
require "game/DayNightCycle"
loadDayNightCycle()
require "game/slowMotion"
loadSlowMotion()
require "game/worldSettings"
loadWorldSettings()
require "game/voices"
loadVoices()
require "game/music"
loadMusic()
require "game/levelAssets"
loadLevelAssets()
require "game/cameraShake"


function lovr.load()
  
    
    loseV = "t"
    timer = 0
    firstGameStarted = false

    hasLoaded = false

  
  loadShadow()
  
  -- Define variables
planetName = "Astronoria"
local timer = 0  -- Initialize the timer to 0
cameraShakeTimer= 0


--camera
cameraShakeIntensity = 0.1
cameraShakeTimer = 0
cameraShakeDuration = 0.1 -- Adjust the duration

end

function  lovr.update(dt)
timer=timer+dt
cameraShakeTimer=cameraShakeTimer+dt

updateDayNightCycle(dt)

--rotation
 -- Calculate the rotation angle based on time
  local rotationSpeed = math.rad(1)  -- Adjust the rotation speed
  local time = lovr.timer.getTime()
  rotationAngle = time * rotationSpeed
	

  if loseV == "f" then
    -- Reduce delta time when slow motion is active
    if slowMotionActive then
      dt = dt * 0.5 -- adjust the factor to control the intensity of the slow-motion
    end
    
	updateVoices(dt)

	updateSlowMotion(dt)

	end
  calcShadow()

  -- Apply inertia to player1's movement
  local inertiaFactor = 1.95

-- Player 1
  if player1.isHoldingW and player1.y + player1.height < world.height then
    player1.speed = player1.speed + (player1.baseSpeed - player1.speed) * inertiaFactor
    player1.y = player1.y + player1.speed * dt
  end

  if player1.isHoldingS and player1.y > 40 then
    player1.speed = player1.speed + (player1.baseSpeed - player1.speed) * inertiaFactor
    player1.y = player1.y - player1.speed * dt
  end
  
  -- Player 2
  if player2.isHoldingW and player2.y + player2.height < world.height then
    player2.speed = player2.speed + (player2.baseSpeed - player2.speed) * inertiaFactor
    player2.y = player2.y + player2.speed * dt
  end

  if player2.isHoldingS and player2.y > 40 then
    player2.speed = player2.speed + (player2.baseSpeed - player2.speed) * inertiaFactor
    player2.y = player2.y - player2.speed * dt
  end

  -- Limit the base speed to the maximum speed
  player1.baseSpeed = math.min(maxPlayerSpeed, player1.baseSpeed)
  player2.baseSpeed = math.min(maxPlayerSpeed, player2.baseSpeed)

  -- Limit the speed to avoid it dropping too low
  player1.speed = math.max(player1.speed, 0)
  player2.speed = math.max(player2.speed, 0)

  -- Check if the ball hits the players
  if checkCollP1() or checkCollP2() then
    -- Activate the slow-motion effect
    slowMotionActive = true
    --timer=0
  end
  
	
  
  if player2.score>15 and not (gamestatus=="GameOver") then
	gamestatus="GameOver"
  end
  
	updateCameraShake(dt)

end




function sayWhosWinning()
		if player1.score>player2.score then
			HumanIsWinning:play()
			CrowdCheering:play()
	elseif player2.score>player1.score then
			CPUIsWinning:play()
			CrowdCheering:play()
		end
end

function drawPlanetsDayLandscape()
 
   		
   lovr.graphics.setColor(1, 1, 1, 1)
  
   -- Calculate time of day
   local timeOfDay = timer / dayDurationSeconds
   timeOfDay = math.fmod(timeOfDay, 1)  -- Wrap around for a continuous day/night cycle

  -- Convert timeOfDay to hours and minutes
   local hours = math.floor(timeOfDay * 24)
   local minutes = math.floor((timeOfDay * 24 * 60) % 60)
   local seconds = math.floor((timeOfDay * 24 * 60 * 60) % 60)
   local timeText = string.format("Time: %02d:%02d:%02d", hours, minutes, seconds)
   
   	if firstGameStarted==false and not (isplayerlostplaying==true) then
		lovr.graphics.print		 ("Planet Name: " .. planetName, 400, 700, -300, 70)
		lovr.graphics.print("Day Duration: " .. (dayDurationSeconds / 3600) .. " hours", 400, 800, -300, 50)
		lovr.graphics.print(timeText, 400, 900, -300, 50)
		lovr.graphics.print(temperatureText, 400, 1000, -300, 50)
	end
	
	if gamestatus=="GameOver" then
		lovr.graphics.print		 ("Game Over", 400, 700, -300, 70)
	end
  
end

function updateSlowMotionEffect()

end

function drawSlowMotionEffect()
 -- Draw a dark vignette around the edges of the screen
    lovr.graphics.setColor(0, 0, 0, 0.4)
    lovr.graphics.plane('fill', -800, -800, 1600, 800) -- Top
    lovr.graphics.plane('fill', -800, -800, 800, 1600) -- Left
    lovr.graphics.plane('fill', -800, 0, 1600, 800) -- Bottom
    lovr.graphics.plane('fill', 0, -800, 800, 1600) -- Right
    
   
    
end

function lovr.draw()

 -- Render the 3D world first with the camera's angle
   if loseV == "t" then
      lovr.graphics.transform(-20, -10, -50, 0.05, 0.05, 0.05, rotationAngle)
   else
	drawSlowMotionEffect()
      lovr.graphics.transform(-20, -10, -50, 0.05, 0.05, 0.05)
      drawPlayer()
      renderShadow()
   --[[elseif not (slowMotionActive==true) then
      lovr.graphics.transform(-20, -10, -50, 0.05, 0.05, 0.05)
      drawPlayer()
      renderShadow()
   elseif slowMotionActive==true then
		lovr.graphics.transform(-20+CameraOffset.x, -10+CameraOffset.y, -50+CameraOffset.z, 0.05, 0.05, 0.05)
		drawPlayer()
		renderShadow()
   
   --]]
   end
	
	
	drawEnvironment()
	drawPlanetsDayLandscape()

  lovr.graphics.setColor(1,1,1,1)
  
  
  if loseV == "1" or loseV == "2" then
    lose()
  else
    if loseV == "f" then
renderShadow()
    end
  end
  
  if loseV == "t" then
		
	lovr.graphics.print("Press -R- to start the game",400,500,-300,100)
	lovr.graphics.print("T: Switch CPU/Human",400,300,-300,100)
  else
    drawBall()  
  end
end



function drawPlayer()
  lovr.graphics.print("Score: " .. player1.score, 10, 800,-300,100)
  
  ps = "CPU"
  
  if player2.isPlaying == "t" then
    ps = "Human"
  end
  
  lovr.graphics.print("Score: " .. player2.score, 800,800,-300,100)
 
  lovr.graphics.print("Player 1: Human",10, 1000,-300,50)
  lovr.graphics.print("Player 2: " .. ps, 800, 1000,-300,50)
  
  lovr.graphics.setColor(player1.colorr,player1.colorg,player1.colorb,0.8)
  lovr.graphics.box(boxmat, player1.x, player1.y,0,player1.width, player1.height,player1.depth)
  
  lovr.graphics.setColor(player2.colorr,player2.colorg,player2.colorb,0.8)
  lovr.graphics.box(boxmat, player2.x, player2.y,0,player1.width, player2.height,player2.depth) end

function drawBall()
  -- Draw the original ball first
  lovr.graphics.setColor(1, 1, 1, 1)
  lovr.graphics.sphere(ball.x, ball.y, 0, ball.height)

  -- Define trail settings
  local trailLength = 5  -- Number of trail copies to draw
  local trailSpacing = 0.02  -- Increase the spacing between trail copies
  local trailAlphaDecay = 0.2  -- Alpha decay factor per copy

  -- Create a table to store past ball positions
  local trailPositions = {}

  -- Draw the ball and its trail copies
for i = 1, trailLength do
  local alpha = 1 - (i - 1) * trailAlphaDecay
  
	if ball.speedx==250 then	lovr.graphics.setColor(1, 1/i, 1/i, alpha)
elseif ball.speedx==-250 then	lovr.graphics.setColor(1/i, 1/i, 1, alpha)
end
  -- Calculate the trail positions based on the ball's position and spacing
  local trailX = ball.x - (i - 1) * ball.speedx * trailSpacing
  local trailY = ball.y - (i - 1) * ball.speedy * trailSpacing

  lovr.graphics.sphere(trailX, trailY, 0, ball.height)
end

-- Restore the color
lovr.graphics.setColor(1, 1, 1, 1)

end



function drawEnvironment()
	--print(skymapColor[1])
	--print(skymapColor[2])
	--print(skymapColor[3])
	
	 --lovr.graphics.setShader(fogShader)
	lovr.graphics.setColor(skymapColor)
	lovr.graphics.skybox(background1tex)

	--lovr.graphics.setShader()

  --wall left
  if ball.speedx==250 		then 	lovr.graphics.setColor(1/timer,1/timer,1,0.3)
  --elseif ball.speedx==-250  then	lovr.graphics.setColor(1,1,1,0.3)
  end
  lovr.graphics.box(boxmat,-250,350,0,10,1000,1500)
  --wall right
  if ball.speedx==-250  then	lovr.graphics.setColor(1,1/timer,1/timer,0.3)
  end
  lovr.graphics.box(boxmat,1100,350,0,10,1000,1500)
  --wall front
    lovr.graphics.setColor(1,1,1,0.06)
  lovr.graphics.box(boxmat,400,380,-700,1500,1000,10)
  
  --floor
  if ball.y < 60 then lovr.graphics.setColor((-ball.y+1),(-ball.y+1),1,0.3)
  else --lovr.graphics.setColor(1,1,1,0.06)
  end
  
  lovr.graphics.box(boxmatflo,400,-200,-300,1600,10,2000)
  --ceiling
   if ball.y > world.height + 30 then lovr.graphics.setColor((-ball.y+1),(-ball.y+1),1,0.3)
  lovr.graphics.box(boxmatcei,400,900,-300,1600,10,2000)
  else --lovr.graphics.setColor(1,1,1,0.06)
  end
  
end

function lose()
  ball.width = 0
  ball.height = 0
  
  lovr.graphics.setColor(1,1,1,1)
  lovr.graphics.print("Player " .. loseV .. " Lost!", 400,300,300,70)
  if not (gamestatus=="GameOver") then
	lovr.graphics.print("to continue the game press -R-", 400,200,300,50)
end
  lovr.graphics.print("To restart the match press -X-", 400,100,300,50)
   if isplayerlostplaying==false then
		if not (player1.score>4 and sentence5PoinsSaid==false) and not (player1.score>9 and sentence10PoinsSaid==false) then
			if loseV=="1" then
				Player1lost= lovr.audio.newSource( "voice/Player1lost.ogg")
				Player1lost:play()
			elseif loseV=="2" then
				Player2lost= lovr.audio.newSource( "voice/Player2lost.ogg")
				Player2lost:play()
			end
		end
		isplayerlostplaying=true
	end
end

function resetBall()
  loseV = "f"
  ball.x = 375
  ball.y = 275
  ball.width = 25
  ball.height = 25
  if firstGameStarted==false then
	timer=0
	firstGameStarted=true
	end
end

function resetMatch()
  player1.score = 0
  player2.score = 0
  
  resetBall()
end

function swapC()
  if player1.colorr == 1 then
    player1.colorr = 0.1
    player1.colorg = 0.1
    player1.colorb = 1
 
    player2.colorr = 1
    player2.colorg = 0.1
    player2.colorb = 0.1
  else
    player1.colorr = 1
    player1.colorg = 0.1
    player1.colorb = 0.1
    
    player2.colorr = 0.1
    player2.colorg = 0.1
    player2.colorb = 1
  end
end


