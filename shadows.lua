function renderShadow()
  lovr.graphics.setColor(1,1,1,0.1)
  
  lovr.graphics.sphere(shadowB.shadex,shadowB.shadey,-800,10)
  lovr.graphics.box(boxmatshadow,20,shadowP1y,-800,player1.width, player1.height,player1.depth)
  lovr.graphics.box(boxmatshadow,745,shadowP2y,-800,player2.width, player2.height,player2.depth)

end

function calcShadow()
  if ball.speedx > 1 then
    shadowB.shadex = ball.x - 5 
  else
    shadowB.shadex = ball.x + 5
  end
  
  if ball.speedy > 1 then
    shadowB.shadey = ball.y - 5
  else
    shadowB.shadey = ball.y + 5
  end
  
    if player1.speed > 1 then
    shadowP1y =  player1.y - 5
  else
    shadowP1y =  player1.y + 5
  end
  
  if player2.speed> 1 then
    shadowP2y =  player2.y - 5
  else
    shadowP2y =  player2.y + 5
  end  
  
end

function loadShadow()
  shadowB = {}
  shadowB.shadex = 0
  shadowB.shadey = 0
end
